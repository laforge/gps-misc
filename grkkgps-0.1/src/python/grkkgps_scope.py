#!/usr/bin/env python
#
# Copyright 2004 Free Software Foundation, Inc.
# 
# This file is part of GNU Radio
# 
# GNU Radio is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
# 
# GNU Radio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with GNU Radio; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.
# 

from gnuradio import gr, gru
from gnuradio import usrp
from gnuradio.eng_option import eng_option
from gnuradio.wxgui import stdgui, fftsink, scopesink
import wx
from gnuradio import grkkgps
from gnuradio import dbs_rx

class app_flow_graph (stdgui.gui_flow_graph):
    def __init__(self, frame, panel, vbox, argv):
        stdgui.gui_flow_graph.__init__ (self, frame, panel, vbox, argv)

#simulated real sources        
        if 1:
	    pn = 2
	    threshold = 0.20
            ifreq = 1.25e6
            sample_rate = 5000000

	    src = gr.add_ff ()
	    self.rx = gr.float_to_complex ()
	    self.connect ( src , (self.rx,0) )		    
            src1 = grkkgps.gps_source_f ( sample_rate , ifreq + 45 , pn , "101100111000" )
#            src2 = grkkgps.gps_source_f ( sample_rate , ifreq + 45 , 2 , "101100111000" )
#            src3 = grkkgps.gps_source_f ( sample_rate , ifreq + 45 , 3 , "101100111000" )
#            src4 = grkkgps.gps_source_f ( sample_rate , ifreq + 45 , 4 , "101100111000" )
#            src5 = grkkgps.gps_source_f ( sample_rate , ifreq + 45 , 5 , "101100111000" )
#            src6 = grkkgps.gps_source_f ( sample_rate , ifreq + 45 , 6 , "101100111000" )
            self.connect( src1 , (src,0))
#            self.connect( src2 , (src,1))
#            self.connect( src3 , (src,2))
#            self.connect( src4 , (src,3))
#            self.connect( src5 , (src,4))
#            self.connect( src6 , (src,5))

#simulated complex sources        
        if 0:
	    pn = 2
	    threshold = 0.001
            ifreq = 1.25e6
            sample_rate = 5000000

	    ifreq_raw = 2.5e6
	    sample_rate_raw = 10000000
            src1 = grkkgps.gps_source_f ( sample_rate_raw , ifreq_raw + 45 , pn , "101100111000" )
	    taps = gr.firdes.low_pass (
	             1.0,                # gain
		     sample_rate_raw,    # sample_rate
		     1.25e6,                # cutoff_frequency
		     0.50e6,              # width_of_transition_band
		     gr.firdes.WIN_HANN) # window
	    if_sig = gr.freq_xlating_fir_filter_fcf ( 2 , taps , -ifreq_raw, sample_rate_raw )
	    self.connect ( src1 , if_sig )		    
	    #standard c to f code
            nco = gr.sig_source_c ( sample_rate, gr.GR_SIN_WAVE, ifreq, 1, 0 )
            self.rx = gr.multiply_cc ()
            self.connect ( if_sig , (self.rx,0) )
            self.connect ( nco , (self.rx,1) )
            src_c2f = gr.complex_to_float ()
            self.connect ( self.rx , src_c2f )
            src = (src_c2f,0)
            

#complex data file source
        if 0:
	    pn = 3
            threshold = 0.4
            sample_rate = 4000000
            ifreq = 1e6
            
	    self.rx = gr.file_source( gr.sizeof_gr_complex , "gps.dat", True )
       	    #standard c to f code
     	    nco = gr.sig_source_c ( sample_rate, gr.GR_SIN_WAVE, ifreq, 1, 0 )
            mult = gr.multiply_cc ()
            self.connect ( self.rx , (mult,0) )
            self.connect ( nco , (mult,1) )
            src_c2f = gr.complex_to_float ()
            self.connect ( mult , src_c2f )
            src = (src_c2f,0)

#complex data file source
        if 0:
	    pn = 3
            threshold = 0.1
            sample_rate = 4092000
            ifreq = 1e6
	    
            if_sig1 = gr.file_source( gr.sizeof_gr_complex , "gps.dat", True )
       	    #standard c to f code
	    nco = gr.sig_source_c ( sample_rate, gr.GR_SIN_WAVE, ifreq, 1, 0 )
	    taps = gr.firdes.low_pass (
	             1.0,                # gain
		     sample_rate,        # sample_rate
		     0.90e6,             # cutoff_frequency
		     0.09e6,             # width_of_transition_band
		     gr.firdes.WIN_HANN) # window
	    if_sig = gr.fir_filter_ccf ( 1 , taps )
	    self.connect( if_sig1 , if_sig )
            self.rx = gr.multiply_cc ()
            self.connect ( if_sig , (self.rx,0) )
            self.connect ( nco , (self.rx,1) )
            src_c2f = gr.complex_to_float ()
            self.connect ( self.rx , src_c2f )
	    orx = self.rx
	    self.rx = gr.float_to_complex ()
	    self.connect ((src_c2f,0),(self.rx,0))
#	    self.connect ((src_c2f,1),(self.rx,1))
	    
            src = (src_c2f,0)

#dbsrx source
        if 0:
	    pn = 10
            threshold = 150
            adc_rate = 64000000
            usrp_decim = 16
            ifreq = 1e6
            self.rx = usrp.source_c ( 0, usrp_decim ) 
            self.rx.set_mux( gru.hexint( 0x32323232 ) )

            self.dbs = dbs_rx.dbs_rx ( self.rx, 1 )

            #we want the center of the GPS freq 1575.42 to be at <ifreq> in the IF signal
            freq = 1575.42 * 1e6 - ifreq 
            #freq = 1575.254 * 1e6
            (success,dbs_freq) = self.dbs.set_freq( freq )
            if not success:
                print "VCO failed to lock to ", freq
            else:
                print "actual freq: ", dbs_freq, " delta freq: ", freq - dbs_freq

            self.dbs.set_gain( 100 )
            self.dbs.set_bw ( 4e6 ) 
            
            sample_rate = adc_rate / usrp_decim

            self.rx.set_rx_freq( 0,  -1 * ( freq - dbs_freq ) )
            src_c2f = gr.complex_to_float()
            self.connect ( self.rx , src_c2f )
            src = (src_c2f,0)



	print "PN: ", pn, "ifreq: ", ifreq

        track = grkkgps.tracking_gps_correlator_fc ( sample_rate , ifreq , pn )
	track.debug_level_set ( 0 )
#	track.debug_level_set ( 1 )
        track.threshold_set( threshold )
        self.connect( src , track )
        
        #display setup
        if 0:
            dev_null = gr.null_sink ( gr.sizeof_gr_complex )
            self.connect ( track , dev_null )
        else:
            scope_c2f_prompt = gr.complex_to_float ()
            scope_c2f_track = gr.complex_to_float ()
            scope = scopesink.scope_sink_f (self, panel, "Correlator Output", 1000)
            scope.win.set_format_plus ()
            scope.win.info.xy = True
            #scope.win.info.scopesink.set_trigger_mode ( gr.gr_TRIG_AUTO )
            vbox.Add (scope.win, 1, wx.EXPAND)
            #display prompt IQ
            self.connect( (track,0), scope_c2f_prompt )
            self.connect( (scope_c2f_prompt,0), (scope,0) )
            self.connect( (scope_c2f_prompt,1), (scope,1) )
            #display track IQ
            #self.connect( (track,1), scope_c2f_track )
            #self.connect( (scope_c2f_track,0), (scope,2) )
            #self.connect( (scope_c2f_track,1), (scope,3) )

            fft = fftsink.fft_sink_c (
                self, panel, title="Intermediate Freq", fft_size=512,
                sample_rate=sample_rate, y_per_div=10, ref_level=100,
                average=False)
            vbox.Add (fft.win, 1, wx.EXPAND)
            self.connect ( self.rx , fft )


def main ():
    app = stdgui.stdapp (app_flow_graph, "USRP O'scope")
    app.MainLoop ()

if __name__ == '__main__':
    main ()
