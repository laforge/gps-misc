#!/usr/bin/env python

import os
import Numeric
from gnuradio import gr, gr_unittest
import grkkgps


class qa_grkkgps (gr_unittest.TestCase):

    def setUp (self):
        self.fg = gr.flow_graph ()

    def tearDown (self):
        self.fg = None

    def base_source_track_test(self, match, src_prn, src_freq, tra_prn, tra_freq):
       	sample_rate = 5000000
    	src = grkkgps.gps_source_f( sample_rate, src_freq, src_prn, "101100111000" )
	hd = gr.head (gr.sizeof_float, sample_rate * 10)
	cor = grkkgps.tracking_gps_correlator_fc( sample_rate, tra_freq, tra_prn )
	cor.threshold_set( 0.20 )
	dst = gr.vector_sink_c ()
        self.fg.connect( src , hd )
	self.fg.connect( hd, cor)
	self.fg.connect( cor, dst)

	self.fg.run ()

        data = dst.data ()

	self.failIf( abs( data[0].real ) > 0.25 )
	self.failIf( abs( data[0].imag ) > 0.25 )
	if match:
	  self.failIf( abs( data[-1].real ) < 0.25 )
	else:
	  self.failIf( abs( data[-1].real ) > 0.25 ) 
	self.failIf( abs( data[-1].imag ) > 0.25 )
 

    def test_001_source_track_match (self):
	self.base_source_track_test(True,1,1.25e6+45,1,1.25e6)

    def test_002_source_track_mismatch_prn (self):
	self.base_source_track_test(False,1,1.25e6,2,1.25e6)

	
	

if __name__ == '__main__':
    gr_unittest.main ()     	
