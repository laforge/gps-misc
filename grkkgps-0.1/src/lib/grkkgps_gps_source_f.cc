/* -*- c++ -*- */
/*
 * Copyright 2004 Free Software Foundation, Inc.
 * 
 * This file is part of GNU Radio
 * 
 * GNU Radio is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 * 
 * GNU Radio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with GNU Radio; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

// copied from gr_vector_sink_f

#include "grkkgps_gps_source_f.h"
#include <gr_io_signature.h>


grkkgps_gps_source_f::grkkgps_gps_source_f (
  double       sample_rate,
  double       intermediate_frequency,
  unsigned int prn_id,
  char const*  message)
: gr_sync_block (
    "gps_source_f",
    gr_make_io_signature ( 0, 0, 0 ),
    gr_make_io_signature ( 1, 1, sizeof(float) ) )
, d_gps_src ( sample_rate, intermediate_frequency, 2046000, prn_id, message )
{
}

//public:

int
grkkgps_gps_source_f::work (
    int noutput_items,
    gr_vector_const_void_star &input_items,
    gr_vector_void_star &output_items)
{
  float* out = static_cast<float*> ( output_items[0] );

  d_gps_src.doWork ( out, out + noutput_items );
  
  return noutput_items;
}


grkkgps_gps_source_f_sptr
grkkgps_make_gps_source_f (
  double       sample_rate,
  double       carrier_frequency,
  unsigned int prn_id,
  char const*  message)
{
  return grkkgps_gps_source_f_sptr ( new grkkgps_gps_source_f ( sample_rate, carrier_frequency, prn_id, message ) );
}
