//---------------------------------------------------------------------------
//Copyright (C) 2003,2004,2005 Krzysztof Kamieniecki (krys@kamieniecki.com)
/*
  This file is part of kkGPS.

  kkGPS is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
 
  kkGPS is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License
  along with kkGPS; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
#include "gps_source.h"
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//GPSSource::
//---------------------------------------------------------------------------
GPSSource::GPSSource(
  f64 const                inSampleRate,
  f64 const                inIntermediateFreq,
  f64 const                inChipRate,
  u32 const                inPrnNumber,
  char const*              inMessage)
: sampleRate_ ( inSampleRate )
, intermediateFreq_ ( inIntermediateFreq )
, chipRate_ ( inChipRate )
, prnNumber_ ( inPrnNumber )
, message_ ( )
, chips_ ( inPrnNumber )
, source_ ( sampleRate_, intermediateFreq_, chipRate_ )
, phase_ ( 0 )
, chipPhase_ ( 2 << 15 )
, messageCodeCount_ ( 0 )
, messageBitCount_ ( 0 )
{
  //fill in message
  for ( u32 i = 0; inMessage[i]; ++i)
    switch ( inMessage[i] )
    {
      case '1':
      case '+': 
        message_.push_back(1);
	break;
      case '0':
      case '-':
	message_.push_back(-1);
	break;
    }
}
//---------------------------------------------------------------------------
void
GPSSource::reset()
{
  phase_            = 0;
  chipPhase_        = 2 << 15;
  messageCodeCount_ = 0;
  messageBitCount_  = 0;
}
//---------------------------------------------------------------------------
void
GPSSource::doWork(
  f32*&                      inBegin,
  f32* const                 inEnd)
{
  while ( inBegin != inEnd )
  {
      //I.F.
      f64 value = *(cosData.cosine(phase_));
      
      //spreading code
      value *= chips_.promptEarly_[((chipPhase_ >> 16) + 0) % 2046 + 2];
      
      //message bit
      value *= message_[messageBitCount_];

      *inBegin = value;
      ++inBegin;
     
      phase_ += source_.carrierDelta_;
      chipPhase_ += source_.chipDelta_;

      //handle updating message bit
      if(chipPhase_ >= (2046 << 16))
      {      	
        ++messageCodeCount_;
        if(messageCodeCount_ >= 20)
        {
          messageCodeCount_ = 0;
          ++messageBitCount_;
          if(messageBitCount_ >= message_.size())
            messageBitCount_ = 0;
        }
      }
      
      chipPhase_ %= 2046 << 16;
    }

}
//---------------------------------------------------------------------------
