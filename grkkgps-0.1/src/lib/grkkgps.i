/* -*- c++ -*- */

%include "exception.i"
%import "gnuradio.i"

%{
#include "gnuradio_swig_bug_workaround.h"
#include "grkkgps_gps_source_f.h"
#include "grkkgps_tracking_gps_correlator_fc.h"
#include <stdexcept>
%}

// --------------------------------------------------------

GR_SWIG_BLOCK_MAGIC(grkkgps,gps_source_f);

grkkgps_gps_source_f_sptr
grkkgps_make_gps_source_f (
    double       sample_rate,
    double       carrier_frequency,
    unsigned int prn_id,
    char const*  message);

class grkkgps_gps_source_f 
: public gr_sync_block 
{
private:  
  grkkgps_gps_source_f (
    double       sample_rate,
    double       carrier_frequency,
    unsigned int prn_id,
    char const*  message);
 
public:
};


// --------------------------------------------------------

GR_SWIG_BLOCK_MAGIC(grkkgps,tracking_gps_correlator_fc);

grkkgps_tracking_gps_correlator_fc_sptr
grkkgps_make_tracking_gps_correlator_fc (
  double       sample_rate,
  double       intermediate_frequency,
  unsigned int prn_id);

class grkkgps_tracking_gps_correlator_fc 
: public gr_block 
{
private:
 //constructor
  grkkgps_tracking_gps_correlator_fc (
    double       sample_rate,
    double       intermediate_frequency,
    unsigned int prn_id);
   
public:

  void
  debug_level_set(
    int const debug_level);

  void
  threshold_set(
    double const threshold);

};


