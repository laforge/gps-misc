# This file was created automatically by SWIG.
# Don't modify this file, modify the SWIG interface instead.
# This file is compatible with both classic and new-style classes.

import _grkkgps

def _swig_setattr_nondynamic(self,class_type,name,value,static=1):
    if (name == "this"):
        if isinstance(value, class_type):
            self.__dict__[name] = value.this
            if hasattr(value,"thisown"): self.__dict__["thisown"] = value.thisown
            del value.thisown
            return
    method = class_type.__swig_setmethods__.get(name,None)
    if method: return method(self,value)
    if (not static) or hasattr(self,name) or (name == "thisown"):
        self.__dict__[name] = value
    else:
        raise AttributeError("You cannot add attributes to %s" % self)

def _swig_setattr(self,class_type,name,value):
    return _swig_setattr_nondynamic(self,class_type,name,value,0)

def _swig_getattr(self,class_type,name):
    method = class_type.__swig_getmethods__.get(name,None)
    if method: return method(self)
    raise AttributeError,name

import types
try:
    _object = types.ObjectType
    _newclass = 1
except AttributeError:
    class _object : pass
    _newclass = 0
del types


class grkkgps_gps_source_f_sptr(_object):
    """Proxy of C++ grkkgps_gps_source_f_sptr class"""
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, grkkgps_gps_source_f_sptr, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, grkkgps_gps_source_f_sptr, name)
    def __repr__(self):
        return "<%s.%s; proxy of C++ boost::shared_ptr<grkkgps_gps_source_f > instance at %s>" % (self.__class__.__module__, self.__class__.__name__, self.this,)
    def __init__(self, *args):
        """
        __init__(self) -> grkkgps_gps_source_f_sptr
        __init__(self,  p) -> grkkgps_gps_source_f_sptr
        """
        _swig_setattr(self, grkkgps_gps_source_f_sptr, 'this', _grkkgps.new_grkkgps_gps_source_f_sptr(*args))
        _swig_setattr(self, grkkgps_gps_source_f_sptr, 'thisown', 1)
    def __deref__(*args):
        """__deref__(self)"""
        return _grkkgps.grkkgps_gps_source_f_sptr___deref__(*args)

    def name(*args):
        """name(self) -> string"""
        return _grkkgps.grkkgps_gps_source_f_sptr_name(*args)

    def input_signature(*args):
        """input_signature(self) -> gr_io_signature_sptr"""
        return _grkkgps.grkkgps_gps_source_f_sptr_input_signature(*args)

    def output_signature(*args):
        """output_signature(self) -> gr_io_signature_sptr"""
        return _grkkgps.grkkgps_gps_source_f_sptr_output_signature(*args)

    def unique_id(*args):
        """unique_id(self) -> long"""
        return _grkkgps.grkkgps_gps_source_f_sptr_unique_id(*args)

    def output_multiple(*args):
        """output_multiple(self) -> int"""
        return _grkkgps.grkkgps_gps_source_f_sptr_output_multiple(*args)

    def relative_rate(*args):
        """relative_rate(self) -> double"""
        return _grkkgps.grkkgps_gps_source_f_sptr_relative_rate(*args)

    def check_topology(*args):
        """check_topology(self, int ninputs, int noutputs) -> bool"""
        return _grkkgps.grkkgps_gps_source_f_sptr_check_topology(*args)

    def start(*args):
        """start(self) -> bool"""
        return _grkkgps.grkkgps_gps_source_f_sptr_start(*args)

    def stop(*args):
        """stop(self) -> bool"""
        return _grkkgps.grkkgps_gps_source_f_sptr_stop(*args)

    def detail(*args):
        """detail(self) -> gr_block_detail_sptr"""
        return _grkkgps.grkkgps_gps_source_f_sptr_detail(*args)

    def set_detail(*args):
        """set_detail(self, gr_block_detail_sptr detail)"""
        return _grkkgps.grkkgps_gps_source_f_sptr_set_detail(*args)

    def __del__(self, destroy=_grkkgps.delete_grkkgps_gps_source_f_sptr):
        """__del__(self)"""
        try:
            if self.thisown: destroy(self)
        except: pass


class grkkgps_gps_source_f_sptrPtr(grkkgps_gps_source_f_sptr):
    def __init__(self, this):
        _swig_setattr(self, grkkgps_gps_source_f_sptr, 'this', this)
        if not hasattr(self,"thisown"): _swig_setattr(self, grkkgps_gps_source_f_sptr, 'thisown', 0)
        _swig_setattr(self, grkkgps_gps_source_f_sptr,self.__class__,grkkgps_gps_source_f_sptr)
_grkkgps.grkkgps_gps_source_f_sptr_swigregister(grkkgps_gps_source_f_sptrPtr)


def grkkgps_gps_source_f_block(*args):
    """grkkgps_gps_source_f_block(grkkgps_gps_source_f_sptr r) -> gr_block_sptr"""
    return _grkkgps.grkkgps_gps_source_f_block(*args)
grkkgps_gps_source_f_sptr.block = lambda self: grkkgps_gps_source_f_block (self)
grkkgps_gps_source_f_sptr.__repr__ = lambda self: "<gr_block %s (%d)>" % (self.name(), self.unique_id ())


def gps_source_f(*args):
    """
    gps_source_f(double sample_rate, double carrier_frequency, unsigned int prn_id, 
        char message) -> grkkgps_gps_source_f_sptr
    """
    return _grkkgps.gps_source_f(*args)
class grkkgps_tracking_gps_correlator_fc_sptr(_object):
    """Proxy of C++ grkkgps_tracking_gps_correlator_fc_sptr class"""
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, grkkgps_tracking_gps_correlator_fc_sptr, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, grkkgps_tracking_gps_correlator_fc_sptr, name)
    def __repr__(self):
        return "<%s.%s; proxy of C++ boost::shared_ptr<grkkgps_tracking_gps_correlator_fc > instance at %s>" % (self.__class__.__module__, self.__class__.__name__, self.this,)
    def __init__(self, *args):
        """
        __init__(self) -> grkkgps_tracking_gps_correlator_fc_sptr
        __init__(self,  p) -> grkkgps_tracking_gps_correlator_fc_sptr
        """
        _swig_setattr(self, grkkgps_tracking_gps_correlator_fc_sptr, 'this', _grkkgps.new_grkkgps_tracking_gps_correlator_fc_sptr(*args))
        _swig_setattr(self, grkkgps_tracking_gps_correlator_fc_sptr, 'thisown', 1)
    def __deref__(*args):
        """__deref__(self)"""
        return _grkkgps.grkkgps_tracking_gps_correlator_fc_sptr___deref__(*args)

    def debug_level_set(*args):
        """debug_level_set(self, int debug_level)"""
        return _grkkgps.grkkgps_tracking_gps_correlator_fc_sptr_debug_level_set(*args)

    def threshold_set(*args):
        """threshold_set(self, double threshold)"""
        return _grkkgps.grkkgps_tracking_gps_correlator_fc_sptr_threshold_set(*args)

    def name(*args):
        """name(self) -> string"""
        return _grkkgps.grkkgps_tracking_gps_correlator_fc_sptr_name(*args)

    def input_signature(*args):
        """input_signature(self) -> gr_io_signature_sptr"""
        return _grkkgps.grkkgps_tracking_gps_correlator_fc_sptr_input_signature(*args)

    def output_signature(*args):
        """output_signature(self) -> gr_io_signature_sptr"""
        return _grkkgps.grkkgps_tracking_gps_correlator_fc_sptr_output_signature(*args)

    def unique_id(*args):
        """unique_id(self) -> long"""
        return _grkkgps.grkkgps_tracking_gps_correlator_fc_sptr_unique_id(*args)

    def output_multiple(*args):
        """output_multiple(self) -> int"""
        return _grkkgps.grkkgps_tracking_gps_correlator_fc_sptr_output_multiple(*args)

    def relative_rate(*args):
        """relative_rate(self) -> double"""
        return _grkkgps.grkkgps_tracking_gps_correlator_fc_sptr_relative_rate(*args)

    def check_topology(*args):
        """check_topology(self, int ninputs, int noutputs) -> bool"""
        return _grkkgps.grkkgps_tracking_gps_correlator_fc_sptr_check_topology(*args)

    def start(*args):
        """start(self) -> bool"""
        return _grkkgps.grkkgps_tracking_gps_correlator_fc_sptr_start(*args)

    def stop(*args):
        """stop(self) -> bool"""
        return _grkkgps.grkkgps_tracking_gps_correlator_fc_sptr_stop(*args)

    def detail(*args):
        """detail(self) -> gr_block_detail_sptr"""
        return _grkkgps.grkkgps_tracking_gps_correlator_fc_sptr_detail(*args)

    def set_detail(*args):
        """set_detail(self, gr_block_detail_sptr detail)"""
        return _grkkgps.grkkgps_tracking_gps_correlator_fc_sptr_set_detail(*args)

    def __del__(self, destroy=_grkkgps.delete_grkkgps_tracking_gps_correlator_fc_sptr):
        """__del__(self)"""
        try:
            if self.thisown: destroy(self)
        except: pass


class grkkgps_tracking_gps_correlator_fc_sptrPtr(grkkgps_tracking_gps_correlator_fc_sptr):
    def __init__(self, this):
        _swig_setattr(self, grkkgps_tracking_gps_correlator_fc_sptr, 'this', this)
        if not hasattr(self,"thisown"): _swig_setattr(self, grkkgps_tracking_gps_correlator_fc_sptr, 'thisown', 0)
        _swig_setattr(self, grkkgps_tracking_gps_correlator_fc_sptr,self.__class__,grkkgps_tracking_gps_correlator_fc_sptr)
_grkkgps.grkkgps_tracking_gps_correlator_fc_sptr_swigregister(grkkgps_tracking_gps_correlator_fc_sptrPtr)


def grkkgps_tracking_gps_correlator_fc_block(*args):
    """grkkgps_tracking_gps_correlator_fc_block(grkkgps_tracking_gps_correlator_fc_sptr r) -> gr_block_sptr"""
    return _grkkgps.grkkgps_tracking_gps_correlator_fc_block(*args)
grkkgps_tracking_gps_correlator_fc_sptr.block = lambda self: grkkgps_tracking_gps_correlator_fc_block (self)
grkkgps_tracking_gps_correlator_fc_sptr.__repr__ = lambda self: "<gr_block %s (%d)>" % (self.name(), self.unique_id ())


def tracking_gps_correlator_fc(*args):
    """
    tracking_gps_correlator_fc(double sample_rate, double intermediate_frequency, 
        unsigned int prn_id) -> grkkgps_tracking_gps_correlator_fc_sptr
    """
    return _grkkgps.tracking_gps_correlator_fc(*args)

