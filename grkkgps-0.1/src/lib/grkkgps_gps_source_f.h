/* -*- c++ -*- */
/*
 * Copyright 2004 Free Software Foundation, Inc.
 * 
 * This file is part of GNU Radio
 * 
 * GNU Radio is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 * 
 * GNU Radio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with GNU Radio; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

// copied from gr_vector_sink_f

#ifndef INCLUDED_GRKKGPS_GPS_SOURCE_F_H
#define INCLUDED_GRKKGPS_GPS_SOURCE_F_H

#include <gr_sync_block.h>
#include "gps_source.h"

class grkkgps_gps_source_f;
typedef boost::shared_ptr<grkkgps_gps_source_f> grkkgps_gps_source_f_sptr;

grkkgps_gps_source_f_sptr
grkkgps_make_gps_source_f (
  double       sample_rate,
  double       carrier_frequency,
  unsigned int prn_id,
  char const*  message);

/*!
 * \brief outputs a gps signal made of a carrier frequency, 50 bits/s message, and a 1000 x 1023 chips/s spreading code.
 * \ingroup gps
 */

class grkkgps_gps_source_f 
: public gr_sync_block 
{
  friend 
  grkkgps_gps_source_f_sptr 
  grkkgps_make_gps_source_f (
    double       sample_rate,
    double       carrier_frequency,
    unsigned int prn_id,
    char const*  message);

  //data members
  GPSSource d_gps_src;
  
  //constructor
  grkkgps_gps_source_f (
    double       sample_rate,
    double       carrier_frequency,
    unsigned int prn_id,
    char const*  message);
 
public:
  //gr required methods
  virtual 
  int 
  work (
      int noutput_items,
      gr_vector_const_void_star &input_items,
      gr_vector_void_star &output_items);

};

#endif
