/* -*- c++ -*- */
/*
 * Copyright 2005 Free Software Foundation, Inc.
 * 
 * This file is part of GNU Radio
 * 
 * GNU Radio is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 * 
 * GNU Radio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with GNU Radio; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

// copied from gr_vector_sink_f

#include "grkkgps_tracking_gps_correlator_fc.h"
#include <gr_io_signature.h>
#include <iostream>


grkkgps_tracking_gps_correlator_fc::grkkgps_tracking_gps_correlator_fc (
  double       sample_rate,
  double       intermediate_frequency,
  unsigned int prn_id)
: gr_block (
    "tracking_gps_correlator_fc",
    gr_make_io_signature ( 1, 1, sizeof(float) ),
    gr_make_io_signature ( 1, 2, sizeof(gr_complex) ) )
, d_gps_corr ( sample_rate, intermediate_frequency, 2046000, prn_id )
{
  set_relative_rate( 1000.0 / sample_rate ); //there are 1000 symbols per second 20 chips/bit * 50 bits/second
  
  d_gps_corr.start();
}

//public:
int
grkkgps_tracking_gps_correlator_fc::general_work (
    int noutput_items,
    gr_vector_int &ninput_items, 
    gr_vector_const_void_star &input_items, 
    gr_vector_void_star &output_items)
{
  int out_cnt = 0;
  gr_complex* out_prompt_b = 0;
  gr_complex* out_track_b = 0;
  
  if(output_items.size() > 0)
    out_prompt_b = static_cast<gr_complex*>( output_items[0] );

  if(output_items.size() > 1)
    out_track_b = static_cast<gr_complex*>( output_items[1] );
 

  int const in_cnt = ninput_items[0];
  float const* const in_b = static_cast<float const*> ( input_items[0] );
  float const* const in_e = in_b + in_cnt;
  float const* curr = in_b;
  
  while(out_cnt < std::min(noutput_items,1)) //out_cnt < noutput_items
  {
    if(curr == in_e)
      break;
    
    bool const result = d_gps_corr.doTrack ( curr, in_e );

    if(result)
    {
      if(out_prompt_b) out_prompt_b[out_cnt] = gr_complex( d_gps_corr.dumpedSums_.ip_, d_gps_corr.dumpedSums_.qp_ );
      if(out_track_b) out_track_b[out_cnt] = gr_complex( d_gps_corr.dumpedSums_.it_, d_gps_corr.dumpedSums_.qt_ );
      ++out_cnt;
      
      //debug dump info
#if 0
      std::cout << "ip qp it qt:" 
	        << " " << d_gps_corr.dumpedSums_.ip_
		<< " " << d_gps_corr.dumpedSums_.qp_
		<< " " << d_gps_corr.dumpedSums_.it_
		<< " " << d_gps_corr.dumpedSums_.qt_
		<< "\n";
#endif

#if 0
      std::cout << "Tracking state:" 
	        << " " << d_gps_corr.trackingState_ 
		<< "\n";

      std::cout << "DopplerFreq:"
	        << " " << 1.0 * d_gps_corr.carrierDelta_ * d_gps_corr.sampleRate_ / std::pow(2.0,32) - d_gps_corr.intermediateFreq_
		<< "\n";
#endif
    }
   
  }

  consume_each(in_cnt - (in_e - curr));
  
  return out_cnt;
}

void
grkkgps_tracking_gps_correlator_fc::debug_level_set(
  int const debug_level)
{
  d_gps_corr.debug_level_set(debug_level);
}

void
grkkgps_tracking_gps_correlator_fc::threshold_set(
  double const threshold)
{
  d_gps_corr.threshold_set(threshold);
}

grkkgps_tracking_gps_correlator_fc_sptr
grkkgps_make_tracking_gps_correlator_fc (
  double       sample_rate,
  double       intermediate_frequency,
  unsigned int prn_id)
{
  return grkkgps_tracking_gps_correlator_fc_sptr ( new grkkgps_tracking_gps_correlator_fc ( sample_rate, intermediate_frequency, prn_id ) );
}
