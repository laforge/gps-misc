//---------------------------------------------------------------------------
//Copyright (C) 2003,2004,2005 Krzysztof Kamieniecki (krys@kamieniecki.com)
/*
  This file is part of kkGPS.

  kkGPS is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
 
  kkGPS is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License
  along with kkGPS; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
#ifndef gps_sourceH
#define gps_sourceH
//---------------------------------------------------------------------------
#include <vector>
#include <cmath>
#include "kkutils.h"
#include "gps_corr.h"
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
class
GPSSource
{
public:
  GPSSource(
    f64 const                inSampleRate,
    f64 const                inIntermediateFreq,
    f64 const                inChipRate,
    u32 const                inPrnNumber,
    char const*              inMessage);

  void
  reset();
  
  void
  doWork(
    f32*&                    inBegin,
    f32* const               inEnd);

  //setup variables
  f64                        sampleRate_;
  f64                        intermediateFreq_;
  f64                        chipRate_;
  u32                        prnNumber_;
  std::vector<s8>            message_;
 
  //state variables
  GPSCorrelatorChips         chips_;
  GPSCorrelatorState         source_;
  u32                        phase_; 
  u32                        chipPhase_;
  u32                        messageCodeCount_;
  u32                        messageBitCount_;
 
};
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
#endif
