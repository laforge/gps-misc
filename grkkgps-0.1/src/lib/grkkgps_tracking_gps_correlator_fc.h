/* -*- c++ -*- */
/*
 * Copyright 2005 Free Software Foundation, Inc.
 * 
 * This file is part of GNU Radio
 * 
 * GNU Radio is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 * 
 * GNU Radio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with GNU Radio; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

// copied from gr_vector_sink_f

#ifndef INCLUDED_GRKKGPS_TRACKING_GPS_CORRELATOR_FC_H
#define INCLUDED_GRKKGPS_TRACKING_GPS_CORRELATOR_FC_H

#include <gr_sync_block.h>
#include "gps_corr.h"

class grkkgps_tracking_gps_correlator_fc;
typedef boost::shared_ptr<grkkgps_tracking_gps_correlator_fc> grkkgps_tracking_gps_correlator_fc_sptr;

grkkgps_tracking_gps_correlator_fc_sptr
grkkgps_make_tracking_gps_correlator_fc (
  double       sample_rate,
  double       intermediate_frequency,
  unsigned int prn_id);

/*!
 * \brief A correlator that should acquire and then track a particular PN encoded GPS signal
 * \ingroup gps
 */

class grkkgps_tracking_gps_correlator_fc 
: public gr_block 
{
private:
  friend 
  grkkgps_tracking_gps_correlator_fc_sptr 
  grkkgps_make_tracking_gps_correlator_fc (
    double       sample_rate,
    double       intermediate_frequency,
    unsigned int prn_id);
   
  //data members
  GPSCorrelatorTrack d_gps_corr;
  
  //constructor
  grkkgps_tracking_gps_correlator_fc (
    double       sample_rate,
    double       intermediate_frequency,
    unsigned int prn_id);
   
public:
  //gr required methods
  virtual
  int
  general_work (
      int noutput_items, 
      gr_vector_int &ninput_items, 
      gr_vector_const_void_star &input_items, 
      gr_vector_void_star &output_items);

  void
  debug_level_set(
    int const debug_level);
  
  void
  threshold_set(
    double const threshold);

};

#endif
